package org.sonatype.maven.plugins.upload;

import java.io.File;

import org.apache.commons.httpclient.HttpClient;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;


/**
 * Uploads file to remote repository.
 * 
 * @goal upload-file
 */

public class FileUploadMojo
    extends AbstractUploadMojo
{
    /** @parameter expression="${upload.file}" */
    private File file;

    /** @parameter expression="${upload.repositoryPath}" */
    private String repositoryPath;

    /** @parameter default-value="false" */
    private boolean ignoreMissingFile;

    public void execute()
        throws MojoExecutionException, MojoFailureException
    {
        if ( ignoreMissingFile && !file.exists() )
        {
            getLog().info( "File does not exist, ignoring " + file.getAbsolutePath() );
            return;
        }
        getLog().info( "Found the file cucumber.json file" );

      //  ArtifactRepository repository = getArtifactRepository();

        HttpClient client = getHttpClient();

        String url = getTargetUrl();

        getLog().info( "Upload target url: " + url );

        uploadFile( client, file, url );
    }

    private String getTargetUrl()
    {
        StringBuilder sb = new StringBuilder(repositoryPath);

       /* if ( !repository.getUrl().endsWith( "/" ) && !repositoryPath.startsWith( "/" ) )
        {
            sb.append( "/" );
        }*/

       // sb.append( repositoryPath );

        return sb.toString();
    }

}
