package org.sonatype.maven.plugins.upload;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.repository.ArtifactRepositoryPolicy;
import org.apache.maven.artifact.repository.Authentication;
import org.apache.maven.artifact.repository.layout.ArtifactRepositoryLayout;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.repository.Proxy;
import org.apache.maven.repository.RepositorySystem;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;

public abstract class AbstractUploadMojo
    extends AbstractMojo
{
    /** @component */
    protected RepositorySystem repositorySystem;

    /** @component */
    protected ArtifactRepositoryLayout repositoryLayout;

    /** @parameter expression="${session}" */
    protected MavenSession session;

    /** @parameter expression="${upload.serverId}" */
    protected String serverId;

    /** @parameter expression="${upload.repositoryUrl}" */
    protected String repositoryUrl;

    protected HttpClient getHttpClient()
        throws MojoExecutionException
    {
        HttpClient client = new HttpClient();

      /*  Authentication authentication = repository.getAuthentication();
        if ( authentication != null )
        {
            client.getState().setCredentials(
                                              AuthScope.ANY,
                                              new UsernamePasswordCredentials( authentication.getUsername(),
                                                                               authentication.getPassword() ) );

            // workaround for https://issues.sonatype.org/browse/NXCM-1321
            client.getParams().setAuthenticationPreemptive( true );
        }

        Proxy proxy = repository.getProxy();
        if ( proxy != null )
        {
            throw new MojoExecutionException( "Proxy is not supporyed yet" );
        }*/
        return client;
    }

    protected ArtifactRepository getArtifactRepository()
    {
        ArtifactRepositoryPolicy policy = new ArtifactRepositoryPolicy();
        ArtifactRepository repository =
            repositorySystem.createArtifactRepository( serverId, repositoryUrl, repositoryLayout, policy, policy );

        List<ArtifactRepository> repositories = new ArrayList<ArtifactRepository>();
        repositories.add( repository );

        // repositorySystem.injectMirror( artifactRepositories, session.getRequest().getMirrors() );

        repositorySystem.injectProxy( repositories, session.getRequest().getProxies() );

        repositorySystem.injectAuthentication( repositories, session.getRequest().getServers() );

        repository = repositories.get( 0 );
        return repository;
    }

    protected void uploadFile( HttpClient client, File file, String targetUrl )
        throws MojoExecutionException
    {
        getLog().info( "Generating token ");
        String authorizationToken = getFirebaseToken();
        getLog().info( "Generated token "+authorizationToken);
        getLog().info( "Uploading " + file.getAbsolutePath() + " to " + targetUrl );
        PostMethod putMethod = new PostMethod( targetUrl );
        putMethod.addRequestHeader("Authorization","Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjgyZTZiMWM5MjFmYTg2NzcwZjNkNTBjMTJjMTVkNmVhY2E4ZjBkMzUiLCJ0eXAiOiJKV1QifQ.eyJuYW1lIjoiU3VwZXIgQWRtaW4iLCJyb2xlIjoiQWRtaW4iLCJmaXJzdE5hbWUiOiJTdXBlciIsImxhc3ROYW1lIjoiQWRtaW4iLCJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vdHctdHJlZSIsImF1ZCI6InR3LXRyZWUiLCJhdXRoX3RpbWUiOjE1ODQ0NDI2MDIsInVzZXJfaWQiOiI4Qmdja1pNUURoT0I5cEg3cHhtbFJiMlFuVGsyIiwic3ViIjoiOEJnY2taTVFEaE9COXBIN3B4bWxSYjJRblRrMiIsImlhdCI6MTU4NDQ0MjYwMiwiZXhwIjoxNTg0NDQ2MjAyLCJlbWFpbCI6InRlc3RAdGVzdC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsidGVzdEB0ZXN0LmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.SQU7Qey8he6BVi1tdpyjjtudk25pPQ66c07Ia3je9JNbWMPthxl86eAVIDVzJsIGQvk6E9_zohc5HrA2R2cm0neXancaiN9YS7_xPxZsIdN8N5A_l8Ckx-x5GLvE-78msVWtnXREKDbrvXLbylMX9GrPi-7_91fNe_ywDVDeyiOUZjl8LvZxgtYleveEmlYND9uwpFS7XBzCA97K_xuirzQ8pRQNubAeIRYJ0qxhzpycyyWDjwvwouFNHp87GRq_ShSdqAcuOF5__4RS7Eew14PGYS8LA6ugrN1g90l0zzOrmXZsFmRUkhY55f9C1hhukvqiS-Kpar93-tLTYgJvLA");
        putMethod.addRequestHeader("ContentType","application/json");

        try
        {
            String contentType = "application/json";
            if ( file.getName().endsWith( ".xml" ) )
            {
                contentType = "application/xml";
            }
            putMethod.setRequestEntity( new InputStreamRequestEntity( new FileInputStream( file ), contentType ) );

            client.executeMethod( putMethod );

            int status = putMethod.getStatusCode();
            getLog().info( "Status code: " + status );
            if ( status < 200 || status > 299 )
            {
                String message = "Could not upload file: " + putMethod.getStatusLine().toString();
                getLog().error( message );
                String responseBody = putMethod.getResponseBodyAsString();
                if ( responseBody != null )
                {
                    getLog().info( responseBody );
                }
                throw new MojoExecutionException( message );
            }
        }
        catch ( IOException e )
        {
            throw new MojoExecutionException( "Could not upload file: ", e );
        }
        finally
        {
            putMethod.releaseConnection();
        }
    }


    public String getFirebaseToken() throws MojoExecutionException {
        getLog().info( "Calling firebased to get the token");
        PostMethod postMethod = new PostMethod("https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyDsT7cVRVh8fLVy4hfD_szWdWItaCaMn80");
       // postMethod.setRequestBody("{\"email\":\"test@test.com\",\"password\":\"Test@123\",\"returnSecureToken\":true}");
        String responseBody;
        try
        {
            try {
                getHttpClient().executeMethod(postMethod);
            } catch (MojoExecutionException e) {
                e.printStackTrace();
            }

            int status = postMethod.getStatusCode();
            responseBody =  postMethod.getResponseBodyAsString();
            System.out.println(responseBody);
            getLog().info( "Status code: " + status );
            if ( status < 200 || status > 299 )
            {
                String message = "Could not get api token: " + postMethod.getStatusLine().toString();
                getLog().error( message );


            }
        }
        catch ( IOException e )
        {
            throw new MojoExecutionException( "Could not upload file: ", e );
        }
        finally
        {
            postMethod.releaseConnection();
        }


        return responseBody;
    }

}
